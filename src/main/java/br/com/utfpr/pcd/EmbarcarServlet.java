package br.com.utfpr.pcd;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by gabri on 07/12/2017.
 */
@WebServlet("/embarcar")
public class EmbarcarServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String nome = req.getParameter("nome");

        Pessoa pessoa = new Pessoa();
        pessoa.setNomePessoa(nome);

        Trem trem = EstacaoContainer.estacao.getTrem();
        boolean embarcado = trem.embarcar(pessoa);

        PrintWriter printWriter = resp.getWriter();
        printWriter.append("");
        resp.sendRedirect("/index.jsp?embarcado=" + embarcado);
    }
}
