
package br.com.utfpr.pcd;

/**
 *
 * @author gabri
 */
public class Trem implements Runnable{

    Vagao[] vagoes = new Vagao[5];
    private String destino;
    private int idTrem;
    private boolean isEmMovimento = false;

    public Trem(int idTrem, String destino) {
        
        this.idTrem = idTrem;
        this.destino = destino;        
        
    }
    
    public synchronized   boolean embarcar(Pessoa pessoa){
        boolean embarcou = false;
        for (int i=0; i<vagoes.length; i++) {
            System.out.println("Verificando vagao " + i + " para embarcar.");
            if(vagoes[i] == null) {
                vagoes[i] = new Vagao();
            }
            if (vagoes[i].embarcar(pessoa)) {
                embarcou = true;
                break;
            }
        }
        if(!embarcou){
            System.out.println("Trem " + idTrem + " lotado, por isso a pessoa " + 
                    pessoa.getNomePessoa() + " não pode entrar.");
        }
        
        return embarcou;
    }
    
    public void desembarcar(String p){
        for (Vagao vagao : vagoes) {
            vagao.desembarcar(p);
        }
    }

    public Vagao[] getVagoes() {
        return vagoes;
    }

    public boolean isIsEmMovimento() {
        return isEmMovimento;
    }

    public void setIsEmMovimento(boolean isEmMovimento) {
        this.isEmMovimento = isEmMovimento;
    }


    public void run() {
        while(true){
            try {
                Estacao.entrarNaEstacao(this);
                Thread.sleep(10000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setVagoes(Vagao[] vagoes) {
        this.vagoes = vagoes;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public int getIdTrem() {
        return idTrem;
    }

    public void setIdTrem(int idTrem) {
        this.idTrem = idTrem;
    }

    public boolean isEmMovimento() {
        return isEmMovimento;
    }

    public void setEmMovimento(boolean emMovimento) {
        isEmMovimento = emMovimento;
    }
}
