package br.com.utfpr.pcd;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by gabriel.paula on 07/12/2017.
 */
@WebServlet("/desembarcar")
public class DesembarcarServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Trem trem = EstacaoContainer.estacao.getTrem();
        if(Integer.parseInt(request.getParameter("idtrem")) == trem.getIdTrem()){
            trem.desembarcar(request.getParameter("nome"));
        }

    }
}
