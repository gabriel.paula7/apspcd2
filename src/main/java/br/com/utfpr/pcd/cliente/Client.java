package br.com.utfpr.pcd.cliente;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * Created by gabri on 07/12/2017.
 */
public class Client {

    public static void main(String[] args) {

        new MessageReceiver().run();

    }


    static class MessageReceiver extends Thread{
        @Override
        public void run() {
            MulticastSocket socket = null;
            byte[] buf = new byte[256];
            try {
                socket = new MulticastSocket(4446);
                InetAddress group = InetAddress.getByName("230.0.0.0");
                socket.joinGroup(group);
                while (true) {
                    DatagramPacket packet = new DatagramPacket(buf, buf.length);
                    socket.receive(packet);
                    String received = new String(packet.getData(), 0, packet.getLength());
                    System.out.println(received);
                    if ("end".equals(received)) {
                    break;
                    }
                }
                socket.leaveGroup(group);
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
