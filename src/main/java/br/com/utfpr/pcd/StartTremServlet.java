package br.com.utfpr.pcd;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by gabri on 07/12/2017.
 */
@WebServlet("/start")
public class StartTremServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EstacaoContainer.estacao.initTrens();
        response.getOutputStream().println("Trens Startados");
        response.sendRedirect("/index.jsp");
    }
}
