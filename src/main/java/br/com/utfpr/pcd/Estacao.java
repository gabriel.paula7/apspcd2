
package br.com.utfpr.pcd;


import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author gabri
 */
public class Estacao {
    
    private static String nomeEstacao;
    
    private static Trem trem;
    private static LocalTime horaChegada;
    
    public Estacao(String nomeEstacao) {
        this.nomeEstacao = nomeEstacao;
        initTrens();
    }
        
    public static synchronized  void entrarNaEstacao(Trem t){
        trem = t;
        trem.setIsEmMovimento(false);
        horaChegada = LocalTime.now();
        //Servidor.enviarInfoTrem("O trem " + trem.getIdTrem() + " ta na estacao " + nomeEstacao );
        System.out.println("O trem " + trem.getIdTrem() + " ta na estacao " + nomeEstacao );
        try {
            EnvioMsgMulticast.multicast("O trem " + trem.getIdTrem() + " ta na estacao " + nomeEstacao );
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(Duration.between(horaChegada, LocalTime.now()).getSeconds() < 10){

        }
        String msg = "O trem " + trem.getIdTrem() + " ta saindo da estacao " + nomeEstacao;
        try {
            EnvioMsgMulticast.multicast(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Servidor.enviarInfoTrem();

        System.out.println("O trem ta saindo da estacao " + nomeEstacao );
    }


    public void initTrens(){
        ExecutorService threadExecutor = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            threadExecutor.execute(new Trem(i, "Destino " + i));
        }
    }


    public static Trem getTrem() {
        return trem;
    }
}
