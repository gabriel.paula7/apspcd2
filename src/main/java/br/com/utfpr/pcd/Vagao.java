/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.utfpr.pcd;

/**
 *
 * @author gabri
 */
public class Vagao {
    
       
    private Pessoa[] acentos = new Pessoa[20];


    public synchronized boolean embarcar(Pessoa pessoa){
        
        boolean embarcou = false;
        
        for (int i = 0; i < acentos.length; i++) {
            Pessoa acento = acentos[i];
            if(acento == null && acento.getNomePessoa().equals("")){
                acentos[i] = pessoa;
                embarcou = true;
                System.out.println(pessoa.getNomePessoa() + " sentou no acento " + i);
                break;
            } 
        }
        
        return embarcou;
    }
    
    public void desembarcar(String p){
        for (int i = 0; i < acentos.length; i++) {
            Pessoa acento = acentos[i];
            if(p.equals(acento.getNomePessoa())){
                acentos[i] = new Pessoa();
            }
        }
    }
    
}
